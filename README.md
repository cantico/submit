# Submit #

This addon can be used from HTML to submit a form to an email address.

The default recipient is the site administrator.

The email will contain all submited inputs with an attached label also submited in a hidden field, for example :

```html
<label for="contentId">Veuillez saisir votre contenu</label>
<textarea id="contentId" name="data[content][value]">blabla</textarea>
<input type="hidden" name="data[content][name]" value="Contenu saisi par l'utilisateur" />
```

In this case, the email will contain a row with :

```
Contenu saisi par l'utilisateur : blabla
```


data can be categorized with headers using the same method:

```html
<input type="hidden" name="data[name]" value="Mon titre" />
```

Two hidden fields are required :
```html
<input type="hidden" name="tg" value="addon/submit/email" />
<input type="hidden" name="babCsrfProtect" value="{OFGetCsrfProtectToken}" />
```

There is a full example in skins/ovidentia/ovml/contact.html


## Fields example

```

subject = 'Subject'
data\[name] = 'Main Title'
data\[lastname]\[name] = 'Lastname'
data\[lastname]\[value] = ''
data\[firstname]\[name] = 'Firstname'
data\[firstname]\[value] = ''
data\[address]\[name] = 'Adress Section title'
data\[address]\[street]\[name] = 'Street'
data\[address]\[street]\[value] = ''
data\[address]\[city]\[name] = 'City'
data\[address]\[city]\[value] = ''

```


## Special fields

All fields are optional

subject : email subject

successMessage : A text for the next page message

nextPage : url to the next page
